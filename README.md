# vagrant-boxes

This project is used to create boxes to upload to [Vagrant cloud](https://app.vagrantup.com/c2platform/). The boxes are not created from scratch. This project uses existing public boxes and then updates those boxes using shell and / or Ansible provisoners.

## Install roles / collections

```bash
ansible-galaxy collection install -r collections/requirements.yml -p .
ansible-galaxy install -r roles/requirements.yml --force --no-deps -p roles/external
```

## Vagrant images

|Box                     |Versions|Provider  |OS                           |Vagrantfile |
|------------------------|--------|----------|-----------------------------|------------|
|c2platform/ubuntu-bionic|0.1.0   |LXC / LXD |Ubuntu 18.04.6 LTS           |ubuntu18-lxd|
|c2platform/ubuntu-focal |0.1.0   |LXC / LXD |Ubuntu 20.04.5               |ubuntu20-lxd|
|c2platform/ubuntu-jammy |0.1.0   |LXC / LXD |Ubuntu 22.04.1               |ubuntu22-lxd|
|c2platform/centos7      |0.1.0   |LXC / LXD |CentOS Linux release 7.9.2009|centos7-lxd |
|c2platform/centos7      |0.1.0   |VirtualBox|CentOS Linux release 7.9.2009|centos7     |
|c2platform/ubuntu-bionic|0.1.0   |VirtualBox|Ubuntu 18.04.6 LTS           |ubuntu18    |
|c2platform/win2016      |0.1.0   |VirtualBox|MS Windows Server 2016       |win2016     |
|c2platform/awx          |0.1.0   |VirtualBox|Ubuntu 22.04.1               |awx         |

See [Vagrant cloud](https://app.vagrantup.com/c2platform).
